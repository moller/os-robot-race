#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"

// UNIX //////////////////////////////////////////
#include <unistd.h>

#define Sleep(msec) usleep(( msec ) * 1000 )
//////////////////////////////////////////////////

/*
int wait_for_motors() {
    while (ev3_tacho_init() < 1) Sleep(1000);
    printf("Found tacho motors:\n");
    for (int i = 0; i < DESC_LIMIT; i++) {
        if (ev3_tacho[i].type_inx != TACHO_TYPE__NONE_) {
            printf("  type = %s\n", ev3_tacho_type(ev3_tacho[i].type_inx));
            printf("  port = %s\n", ev3_tacho_port_name(i, s));
            printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
        }
    }
}*/

#define LEFT 66
#define RIGHT 67

enum { L, R };
uint8_t motor[ 3 ] = { DESC_LIMIT, DESC_LIMIT, DESC_LIMIT };

void go_forward(int ms, uint8_t sn, FLAGS_T state, float speedLeft, float speedRight) {
    // starts left
   if ( ev3_search_tacho_plugged_in(LEFT,0, &sn, 0 )){
       int max_speed;

      printf( "LEGO_EV3_M_MOTOR 1 is found, run for 5 sec...\n" );
      get_tacho_max_speed( sn, &max_speed );
      printf("  max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( sn, TACHO_COAST );
      set_tacho_speed_sp( sn, max_speed * speedLeft );
      set_tacho_time_sp( sn, ms );
      //set_tacho_ramp_up_sp( sn, 2000 );
      //set_tacho_ramp_down_sp( sn, 2000 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
   } 
   else {
      printf( "Left port not found\n" );
    }
   if (ev3_search_tacho_plugged_in(RIGHT,0, &sn, 0 )) {
      int max_speed;

      printf( "LEGO_EV3_M_MOTOR 1 is found, run for 5 sec...\n" );
      get_tacho_max_speed( sn, &max_speed );
      printf("max speed = %d\n", max_speed );
      set_tacho_stop_action_inx( sn, TACHO_COAST );
      set_tacho_speed_sp( sn, max_speed * speedRight );
      set_tacho_time_sp( sn, ms );
      //set_tacho_ramp_up_sp( sn, 2000 );
      //set_tacho_ramp_down_sp( sn, 2000 );
      set_tacho_command_inx( sn, TACHO_RUN_TIMED );
     
   }
   else{
       printf("Right motor not found");
   }
   Sleep(1.2*ms);
}
// does a left curve
void go_left(int ms, uint8_t sn, FLAGS_T state, float degree) {
    float basespeed = 1.0f;

    go_forward(ms, sn, state, degree, basespeed);
}

// does a right curve
void go_right(int ms, uint8_t sn, FLAGS_T state, float degree) {
    float basespeed = 1.0f;

    go_forward(ms, sn, state, degree, basespeed);
}

// robot stands still and does a right turn
void turn_right(uint8_t sn, FLAGS_T state, float degree) {
    // ToDo calc degree = time * speed ??
    int ms = 900;

    go_forward(ms, sn, state, 1, 0);
}
// robot stands still and does a left turn
void turn_left(uint8_t sn, FLAGS_T state, float degree) {
    int ms = 900;

    go_forward(ms, sn, state, 0, 1);
}

void slow_down(uint8_t sn, FLAGS_T state, int factor){
    if ( ev3_search_tacho_plugged_in(LEFT,0, &sn, 0 )){
        int max_speed;

        get_tacho_max_speed( sn, &max_speed );
        Sleep( 100 );
        do {
            get_tacho_state_flags( sn, &state );
        } while ( state );
        printf( "run to relative position...\n" );
        set_tacho_speed_sp( sn, max_speed / factor );
        set_tacho_ramp_up_sp( sn, 0 );
        set_tacho_ramp_down_sp( sn, 0 );
        set_tacho_position_sp( sn, 90 );
        for ( int i = 0; i < 8; i++ ) {
            set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
            Sleep( 500 );
        }
    }
    if ( ev3_search_tacho_plugged_in(RIGHT,0, &sn, 0 )){
        int max_speed;

        get_tacho_max_speed( sn, &max_speed );
        Sleep( 100 );
        do {
            get_tacho_state_flags( sn, &state );
        } while ( state );
        printf( "run to relative position...\n" );
        set_tacho_speed_sp( sn, max_speed / factor );
        set_tacho_ramp_up_sp( sn, 0 );
        set_tacho_ramp_down_sp( sn, 0 );
        set_tacho_position_sp( sn, 90 );
        for ( int i = 0; i < 8; i++ ) {
            set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
            Sleep( 500 );
        }
    }
}

/***
 * Simulates doing a lab with no obstacles.
 * Doesn't check for its positioning
 ***/
void run_lap(uint8_t sn, FLAGS_T state){
  int ms_start = 2500;
  int ms_length = 4000;
  int ms_width = 2000;
  go_forward(ms_start, sn, state, 0.95, 1.0); // length
  turn_right(sn, state, 0); 
  go_forward(ms_width, sn, state, 0.95, 1.0); // width
  turn_right(sn, state, 0);
  go_forward(ms_length, sn, state, 0.95, 1.0); // length
  turn_right(sn, state, 0);
  go_forward(ms_width, sn, state, 0.95, 1.0); // width
  turn_right(sn, state, 0);
  go_forward(ms_start, sn, state, 0.95, 1.0); // length back to finish line
}


int main(void) {
    int i;
  uint8_t sn;
  FLAGS_T state;
  uint8_t sn_touch;
  uint8_t sn_color;
  uint8_t sn_compass;
  uint8_t sn_sonar;
  uint8_t sn_mag;
  char s[ 256 ];
  int val;
  float value;
  uint32_t n, ii;
#ifndef __ARM_ARCH_4T__
  /* Disable auto-detection of the brick (you have to set the correct address below) */
  ev3_brick_addr = "192.168.0.204";

#endif
  if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
  printf( "Waiting tacho is plugged...\n" );

#endif
  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

  printf( "*** ( EV3 ) Hello! ***\n" );

  printf( "Found tacho motors:\n" );
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }
    run_lap(sn, state);
    
}