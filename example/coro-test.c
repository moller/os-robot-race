

#include <stdio.h>
#include "coroutine.h"
#include <signal.h>
#include <unistd.h>

#define Sleep(msec) usleep(( msec ) * 1000 )

int alive;
int should_die;
CORO_CONTEXT(foo);
CORO_CONTEXT(bar);
CORO_CONTEXT(lorem);

CORO_DEFINE(foo) {
    CORO_BEGIN();
    printf("I'm ");
    CORO_END();
}

CORO_DEFINE(bar) {
    CORO_BEGIN();
    if (should_die) {
        alive = should_die = 0;
        printf("dead\n");
    } else printf("alive\n");
    CORO_END();

}

CORO_DEFINE(lorem) {
    CORO_BEGIN();
    CORO_WAIT(should_die);
    printf("I was killed\n");
    CORO_END();
}

void onExit(int exit) {
    printf("onExit(%d)\n", exit);
    should_die = 1;
}

int main() {
    signal(SIGINT, onExit);
    alive = 1;
    printf("*** Hello! ***\n");

    while (alive) {
        CORO_CALL(foo);
        CORO_CALL(bar);
        CORO_CALL(lorem);
        Sleep(1000);
    }
}