#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <math.h>
#include "coroutine.h"
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "ev3_tacho.h"


#define L_MOTOR_PORT      OUTPUT_C
#define L_MOTOR_EXT_PORT  EXT_PORT__NONE_
#define R_MOTOR_PORT      OUTPUT_B
#define R_MOTOR_EXT_PORT  EXT_PORT__NONE_

#define LINEAR_CAPACITY 0.7f // todo: was recorded at 0.4f
#define ANGULAR_CAPACITY 0.8f // todo: tweak this

#define HARD_TURN_DEG 30 // todo: try 45, 60 and 90
#define ANGLE_SLACK_DEG  0.0f
#define ROTATION_DEG 360.0f

#define SAFE_DISTANCE 300.0f
#define SAFE_DISTANCE_SHORT_SIDE 200.0f
#define DANGER_DISTANCE 50.0f

#define Sleep(msec) usleep(( msec ) * 1000 )

enum {
    OK, ERROR
};
enum {
    DEAD, ALIVE
};

enum {
    NARROW = 1,
    THICk = 3,
};

enum {
    L,
    R
};

enum {
    MOVE_NONE,
    MOVE_FORWARD,
    MOVE_BACKWARD,
    TURN_LEFT,
    TURN_RIGHT,
    TURN_ANGLE,
    STEP_BACKWARD,

    DRIVE,
    /* Stopping */
    STOP,
    STOPPING,
    STOPPED,
    /* Gimmicks */
    BEEP,

    LEFT_AVOID,
    RIGHT_AVOID,
};


enum {
    CORRECT = 0,
    ADJUSTING = 1
};

// undercover
int start_position = NARROW;
int action;         /* Current action */
int command;        /* Command for the 'drive' coroutine */
int is_alive;      /* Should the app stay alive */
int max_speed;      /* Tacho motor max speed */
float o_angle;      /* Offset angle of rotation */
float c_angle;      /* Current angle of rotation */
float t_angle;      /* Target angle of rotation */

uint8_t sn_ir;      /* Sensor reference for the IR sensor */
uint8_t sn_gyro;    /* Sensor reference for the gyroscope sensor */
uint8_t motor[3] = {DESC_LIMIT, DESC_LIMIT, DESC_LIMIT};  /* Sequence numbers of motors */

float turn_intensity = 1;
int c_speed[2] = {0, 0};
int t_speed[2] = {0, 0};

int should_increase_safe_distance(int _turn) {
    if (_turn % 4 == start_position) return 1;
    return 0;
}

const char *command_name(int cmd) {
    switch (cmd) {
        case MOVE_FORWARD:
            return "MOVE_FORWARD";
        case MOVE_NONE:
            return "MOVE_NONE";
        case TURN_LEFT:
            return "TURN_LEFT";
        case LEFT_AVOID:
            return "LEFT_AVOID";
        case RIGHT_AVOID:
            return "RIGHT_AVOID";
        case TURN_RIGHT:
            return "TURN_RIGHT";
        case STOP:
            return "STOP";
        default:
            return "unknown";
    }
}

static void motor_set_speed(int l_speed, int r_speed) {
    set_tacho_speed_sp(motor[L], l_speed);
    set_tacho_speed_sp(motor[R], r_speed);
    multi_set_tacho_command_inx(motor, TACHO_RUN_FOREVER);
}

static int motor_is_running() {
    FLAGS_T state = TACHO_STATE__NONE_;
    get_tacho_state_flags(motor[L], &state);
    if (state != TACHO_STATE__NONE_) return (1);
    get_tacho_state_flags(motor[R], &state);
    if (state != TACHO_STATE__NONE_) return (1);
    return (0);
}

static void motor_stop() {
    multi_set_tacho_command_inx(motor, TACHO_STOP);
}

int initialise_application() {
    ev3_sensor_init();
    ev3_tacho_init();

    /* Initialising state */
    command = action = MOVE_NONE;

    /* Initialising motors */
    if (ev3_search_tacho_plugged_in(L_MOTOR_PORT, L_MOTOR_EXT_PORT, motor + L, 0)) {
        get_tacho_max_speed(motor[L], &max_speed);
        set_tacho_command_inx(motor[L], TACHO_RESET);
    } else {
        printf("LEFT motor is NOT found.\n");
        return DEAD;
    }
    if (ev3_search_tacho_plugged_in(R_MOTOR_PORT, R_MOTOR_EXT_PORT, motor + R, 0)) {
        set_tacho_command_inx(motor[R], TACHO_RESET);
    } else {
        printf("RIGHT motor is NOT found.\n");
        return DEAD;
    }

    /* Initialising sensors */
    if (ev3_search_sensor(LEGO_EV3_US, &sn_ir, 0)) {
        printf("IR sensor is found (%d).\n", sn_ir);
        set_sensor_mode_inx(sn_ir, US_US_DIST_CM);
    } else {
        printf("IR sensor is NOT found.\n");
        return DEAD;
    }

    if (ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro, 0)) {
        printf("Gyro sensor is found (%d).\n", sn_gyro);
        set_sensor_mode_inx(sn_gyro, GYRO_GYRO_G_AND_A);
    } else {
        printf("Gyro sensor is NOT found.\n");
        return DEAD;
    }
    return ALIVE;
}

void onExit(int code) {
    is_alive = 0;
    motor_stop();
    while (motor_is_running()) Sleep(100);
    printf("exit(%d)", code);
}

//void beep() {
//    system("beep");
//}

CORO_CONTEXT(handle_gyroscope);

CORO_DEFINE(handle_gyroscope) {
    CORO_LOCAL float i_angle, angle, acc;
    CORO_BEGIN();

    get_sensor_value0(sn_gyro, &i_angle);
    o_angle = 0;
    for (;;) {
        if (!get_sensor_value0(sn_gyro, &angle)) angle = 0.0f;
        if (!get_sensor_value1(sn_gyro, &acc)) acc = 0.0f;
        c_angle = fmodf(angle - i_angle - o_angle, ROTATION_DEG);
        printf("angLe: %3.0f\ti_angle: %3.0f\tc_angle: %3.0f\tt_angle: %3.0f\to_angle: %3.0f\t", angle, i_angle,
               c_angle, t_angle, o_angle);
        CORO_YIELD();
    }
    CORO_END();
}

CORO_CONTEXT(handle_ir_proximity);

int turns = 0;

int is_short_side(int _turns) {
    return _turns % 2;
}

int laps_driven(int _turns) {
    return _turns / 4;
}

CORO_DEFINE(handle_ir_proximity) {

     CORO_LOCAL int front, prox;
    CORO_BEGIN();
    for (;;) {
        /* Waiting self-driving mode */
        CORO_WAIT(mode == MODE_AUTO);
        prox = 0;
        get_sensor_value(0, ir, &prox); // todo: read proximity sensor value
        if (prox == 0) { // todo: proximity is 0, something is in front
            /* Oops! Stop the vehicle */
            command = MOVE_NONE;
        } else if (prox < 20) {  /* Need for detour... */  // todo: proximity is 2, something is in close, turn 30º left
            front = prox;
            /* Look to the left */
            angle = -30;
            do {
                command = TURN_ANGLE;
                CORO_WAIT(command == MOVE_NONE); // todo: wait until we receive a MOVE_NONE command
                prox = 0;
                get_sensor_value(0, ir, &prox);
                if (prox < front) { // todo: is the distance closer than before? turn back and 30º right
                    if (angle < 0) {
                        /* Still looking to the left - look to the right */
                        angle = 60;
                    } else {
                        /* Step back */
                        command = STEP_BACKWARD;
                        CORO_WAIT(command == MOVE_NONE);
                    }
                }
            } while ((prox > 0) && (prox < 40) && (mode == MODE_AUTO));
        } else { // todo: nothing is close, go straigth
            /* Track is clear - Go! */
            command = MOVE_FORWARD;
        }
        CORO_YIELD();
    }
    CORO_END();
    /*
    CORO_LOCAL float prox;
    CORO_BEGIN();

    for (;;) {
        if (!get_sensor_value0(sn_ir, &prox)) prox = 0;
        turn_intensity = 1 + fminf((SAFE_DISTANCE - fminf(SAFE_DISTANCE, prox)) / DANGER_DISTANCE, 1);
        printf("prox: %4.0f\t%s\t", prox, should_increase_safe_distance(turns) ? "increase" : "normal");

        if (command != TURN_LEFT) {
            if (prox < DANGER_DISTANCE) {
                printf("danger\t\t");
                command = STOP;

                // maybe even keep track of when last turn was to see if it is likely to be in a wall
                // keep track of what length we are on; we can use this to know around what relative time its relevant to turn
                // also we could use this info to celebrate on the finish line, do a dance, play a song or something
            } else {
                float turn_distance = (is_short_side(turns) ? SAFE_DISTANCE_SHORT_SIDE : SAFE_DISTANCE) +
                                      (should_increase_safe_distance(turns) ? SAFE_DISTANCE : 0.0f);
                if (prox < turn_distance) {
                    // todo: if on the short end increase the safe distance
                    // todo: can we correct any deviation from perfect 90º direction
                    // todo: increase speed on straights
    //                command = TURN_LEFT;
                    printf("warning\t\t");
                    command = LEFT_AVOID;
                    t_angle = -30;
                    CORO_WAIT(command == MOVE_FORWARD);
                    float n_prox = -1;
                    get_sensor_value0(sn_ir, &n_prox);
                    printf("n_prox: %3.0f\t",n_prox);
                    if (n_prox < turn_distance) {
                        command = LEFT_AVOID;
                        t_angle = -60;
                    }else {

                        Sleep(600);
                        command = RIGHT_AVOID;
                        t_angle = 30;
//                        Sleep(100);
//                        t_angle = -30;

                    }

                    // turn left
                    // if no decent prox turn right
                    // wait n sec
                    // turn back
                } else {
                    printf("safe\t");
                    command = MOVE_FORWARD;
                }
            }
        }
        CORO_YIELD();
    }
    CORO_END();
    */
}

CORO_CONTEXT(handle_command);

CORO_DEFINE(handle_command) {
    CORO_LOCAL float _angle;
    CORO_BEGIN();
    for (;;) {
        CORO_WAIT(action != command);
        switch (command) {
            case MOVE_FORWARD:
                printf("move_forward");
                _angle = o_angle + c_angle;
                o_angle = _angle;
//                o_angle = _angle == ROTATION_DEG ? 0 : _angle;
                t_angle = c_angle = 0;
                break;

            case TURN_LEFT:
                printf("turn_left");
                t_angle = -90;
                turns += 1;
                break;

            case TURN_RIGHT:
                printf("turn_right");
                t_angle = 90;
                turns += 1;
                break;

            case LEFT_AVOID:
                printf("left_avoid");
//                t_angle = -30;
                break;
            case RIGHT_AVOID:
                printf("left_avoid");
//                t_angle = -30;
                break;

            case STOP:
                printf("stop");
                motor_stop();
                CORO_WAIT(!motor_is_running());
                break;
        }
        action = command;
        CORO_YIELD();
    }
    CORO_END();
}

CORO_CONTEXT(handle_motor_balance);

CORO_DEFINE(handle_motor_balance) {
    CORO_LOCAL float d_angle, local_turn_intensity;
    CORO_LOCAL int adjust_left_right = CORRECT;
    CORO_LOCAL int linear_speed, turning_speed, adjusted_speed;
    CORO_BEGIN();

    linear_speed = (int) floorf(LINEAR_CAPACITY * (float) max_speed);
    turning_speed = (int) floorf(ANGULAR_CAPACITY * (float) linear_speed);
    for (;;) {
        if (t_angle - ANGLE_SLACK_DEG > c_angle || c_angle > t_angle + ANGLE_SLACK_DEG) {
            adjust_left_right = ADJUSTING;
            d_angle = t_angle - c_angle;
            local_turn_intensity =
                    (fminf(HARD_TURN_DEG, (float) abs((int) floorf(d_angle))) / HARD_TURN_DEG) * turn_intensity;
            adjusted_speed = linear_speed - (int) floorf((float) turning_speed * local_turn_intensity);
            printf("dangle: %3.0f\t", d_angle);
            if (d_angle > 0) {
                t_speed[L] = linear_speed;
                t_speed[R] = adjusted_speed;
            } else {
                t_speed[L] = adjusted_speed;
                t_speed[R] = linear_speed;
            }
        } else if (adjust_left_right != CORRECT) {
            adjust_left_right = CORRECT;
            command = MOVE_FORWARD;
        } else {
            t_speed[L] = linear_speed;
            t_speed[R] = linear_speed;
        }
        CORO_YIELD();
    }
    CORO_END();
}


CORO_CONTEXT(handle_motor_speed);

CORO_DEFINE(handle_motor_speed) {
    CORO_BEGIN();
    for (;;) {
        CORO_WAIT(c_speed[L] != t_speed[L] || c_speed[R] != t_speed[R]);
        printf("*\t");
        motor_set_speed(t_speed[L], t_speed[R]);
        c_speed[L] = t_speed[L];
        c_speed[R] = t_speed[R];
        CORO_YIELD();
    }
    CORO_END();
}


int main() {
    signal(SIGINT, onExit);
    printf("Waiting the EV3 brick online...\n");
    if (ev3_init() < 1) return ERROR;
    printf("*** ( EV3 ) Hello! ***\n");

    is_alive = initialise_application();
    while (is_alive) {
        printf("action: %s\tspeed: [%4d, %4d]\t", command_name(action), c_speed[L], c_speed[R]);
        printf("turns: %d\t%s\tlaps: %d\t", turns, is_short_side(turns) ? "short" : "long", laps_driven(turns));
        CORO_CALL(handle_ir_proximity);
        CORO_CALL(handle_gyroscope);
        CORO_CALL(handle_motor_balance);
        CORO_CALL(handle_motor_speed);
        CORO_CALL(handle_command);
        printf("\n");
        Sleep(10);
    }
    ev3_uninit();
    printf("*** ( EV3 ) Bye! ***\n");
    return OK;
}