#include <stdio.h>
#include "coroutine.h"
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "ev3_tacho.h"
#include <unistd.h>
#define Sleep(msec) usleep(( msec ) * 1000 )
// store buffer of ms of data

/*
 *                            (10)  (15)  (15)  (10)
 * turn the sensor between: -40  -30º  -15º  +00º  +10º
 */

int angle;    /* Angle of rotation */
uint8_t ir, touch;  /* Sequence numbers of sensors */
int app_alive;

#define SAFE_DISTANCE 200

CORO_CONTEXT(handle_ir_proximity);


CORO_DEFINE(handle_ir_proximity) {
    CORO_LOCAL int front, prox;
    CORO_BEGIN();
    for (;;) {
//        CORO_WAIT(mode == MODE_AUTO);
        prox = 0;
        get_sensor_value(0, ir, &prox);
        printf("get_sensor_value() -> %d\n", prox);
        if (prox == 0) {
            printf("something is real up close; stop\n");
        } else if (prox < SAFE_DISTANCE) {
            front = prox; // save the distance in front
            //angle = -30;
            printf("something is relatively close; turn left?\n");
            do {
//                command = TURN_ANGLE;
//                CORO_WAIT(command == MOVE_NONE); // todo: wait until we receive a MOVE_NONE command
                prox = 0;
                get_sensor_value(0, ir, &prox);
                if (prox < front) { // todo: is the distance closer than before? turn back and 30º right
                    printf("distance on the left is closer than front\n");
                    if (angle < 0) {
                        /* Still looking to the left - look to the right */
                        //angle = 60;
                        printf("we are looking left; look to the right\n");
                    } else {
                        printf("step back to get a better angle?\n");
                        /* Step back */
//                        command = STEP_BACKWARD;
//                        CORO_WAIT(command == MOVE_NONE);
                    }
                }
            } while ((prox > 0) && (prox < 40) /*&& (mode == MODE_AUTO)*/);
//            } while ((prox > 0) && (prox < 40) && (mode == MODE_AUTO));
        } else { // todo: nothing is close, go straigth
            /* Track is clear - Go! */
//            command = MOVE_FORWARD;
            printf("nothing detected in front so go straight\n");
        }
        CORO_YIELD();
    }
    CORO_END();
}


int main() {
    printf("Waiting the EV3 brick online...\n");
//    if (ev3_init() < 1) return 1;
    app_alive = 1;
//
    printf("*** ( EV3 ) Hello! ***\n");
    ev3_sensor_init();
    ev3_tacho_init();
//    app_alive = app_init();
    while (app_alive) {
        CORO_CALL(handle_ir_proximity);
        Sleep(10);
    }
//    ev3_uninit();
    printf("*** ( EV3 ) Bye! ***\n");
    return 0;
}