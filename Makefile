BDIR	= bin
SDIR	= src
ODIR	= obj
LDIR	= logs
IDIR 	= ./ev3dev-c/source/ev3

CC		= gcc
CFLAGS	= -I$(IDIR) -O2 -std=gnu99 -W -Wall -Wno-comment -c
DEPS_ 	= helper/ev3dev.h flow/laps.h motor/motors.h motor/motor_balance.h motor/motor_speed.h sensor/sensors.h sensor/gyroscope.h sensor/proximity.h flow/state.h flow/commands.h gimmick/beep.h gimmick/catapult.h gimmick/speech.h gimmick/gimmicks.h
DEPS	= $(patsubst %,$(SDIR)/%,$(DEPS_))
OBJS_ 	= helper/ev3dev.o flow/laps.o motor/motors.o motor/motor_balance.o motor/motor_speed.o sensor/sensors.o sensor/gyroscope.o sensor/proximity.o flow/state.o flow/commands.o gimmick/beep.o gimmick/catapult.o  gimmick/speech.o gimmick/gimmicks.o main.o
OBJS	= $(patsubst %,$(ODIR)/%,$(OBJS_))
LOGS_  := latest.log $(shell date +%F--%H-%M-%S).log
LOGS    = $(patsubst %,$(LDIR)/%,$(LOGS_))

main: $(OBJS)
	$(CC) -o $(BDIR)/$@ $^ -Wall -lm -lev3dev-c

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

run: main
	bin/main 2>&1 | tee $(LOGS)

stop: $(ODIR)/stop.o
	$(CC) -o $(BDIR)/$@ $^ -Wall -lm -lev3dev-c
	bin/stop

.PHONY: clean

clean:
	rm -rf $(ODIR)/*
	rm -f $(BDIR)/*

dir:
	mkdir -p $(BDIR)
	mkdir -p $(ODIR)
	mkdir -p $(ODIR)/motor
	mkdir -p $(ODIR)/sensor
	mkdir -p $(ODIR)/gimmick
	mkdir -p $(ODIR)/flow
	mkdir -p $(ODIR)/helper

log:
	less $(LDIR)/latest.log
