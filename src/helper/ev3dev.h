//
// Created by Robert Møller on 22/01/2022.
//

#ifndef OS_ROBOT_RACE_EV3DEV_H
#define OS_ROBOT_RACE_EV3DEV_H

#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "ev3_tacho.h"
/* Utilities */
#define Sleep(msec) usleep(( msec ) * 1000 )


#endif //OS_ROBOT_RACE_EV3DEV_H
