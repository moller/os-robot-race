#include "speech.h"

/**
 * Makes the robot say "Hello"
 * @author Robert Moller
 */
void say_hello() {
    system("espeak -a 200 -s 110 \"Hello\" --stdout | aplay");
}

/**
 * Makes the robot say "Ouch!"
 * @author Robert Moller
 */
void say_ouch() {
    system("espeak -a 200 -s 110 \"Ouch!\" --stdout | aplay");
}

void say_one_done() {
    system("espeak -a 200 -s 110 \"Done!\" --stdout | aplay");
}

void say_Completed() {
    system("espeak -a 200 -s 110 \"Complete complete!\" --stdout | aplay");
}


/**
 * Plays a tire screech sound
 * @author Robert Moller
 */
void tire_screech() {
    system("aplay /home/robot/r/audio/tire_screech.mp3");
}
/**
 * Plays a Tokyo Drift song
 * @author Robert Moller
 */
void tokyo_drift() {
    system("aplay /home/robot/r/audio/tokyo-drift.mp3");
}
