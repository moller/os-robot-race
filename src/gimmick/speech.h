//
// Created by Robert Møller on 26/01/2022.
//

#ifndef OS_ROBOT_RACE_SPEECH_H
#define OS_ROBOT_RACE_SPEECH_H

void say_hello();

void say_ouch();

void say_one_done();

void tire_screech();

void tokyo_drift();

#endif //OS_ROBOT_RACE_SPEECH_H
