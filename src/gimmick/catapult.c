
#include "catapult.h"
#include "../motor/motors.h"
#include "../helper/ev3dev.h"


/**
 * @author Team
 * @details Starts the motor on top for a few seconds to release the obstacle and then turns back
 */
void launch_catapult() {
    int time_to_throw = 300;
    int velocity = max_speed * 0.3f;
    set_tacho_speed_sp(motor[C], -velocity);
    set_tacho_time_sp(motor[C], time_to_throw);
    set_tacho_command_inx(motor[C], TACHO_RUN_TIMED);
    Sleep(time_to_throw * 4);
    set_tacho_speed_sp(motor[C], velocity);
    set_tacho_time_sp(motor[C], time_to_throw);
    set_tacho_command_inx(motor[C], TACHO_RUN_TIMED);
}