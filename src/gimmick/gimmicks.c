#include "gimmicks.h"
#include "catapult.h"
#include "speech.h"
#include "coroutine.h"
#include "../sensor/gyroscope.h"
#include "../sensor/proximity.h"
#include "../flow/laps.h"

#define PROJECTILE_COUNT 1

/**
 * @author Robert Moller
 * @details Employ gimmicks based on the context
 */
CORO_DEFINE(handle_gimmicks) {
    CORO_LOCAL int turn, turn_c = 0, is_playing_music, lap, throws;
    CORO_BEGIN();
    throws = 0;
    for (;;) {
        turn = turns_taken();
        lap = laps_driven();
        printf("turn %d\tlap: %d\t", turn, lap);
        if (race_mode == MALICIOUS) {
            /** If we have completed 2 turns and it still has obstacle */
//            if (turn % 4 == 2 && throws <= PROJECTILE_COUNT) {
//                launch_catapult();
//                throws += 1;
//            }
        }

        /** If something is real lose*/
//        if (DANGER_DISTANCE > c_prox || c_prox >= 2000) say_ouch();

        /** For each turn */
        if (turn > turn_c) {
            turn_c += 1;
//            tire_screech();
        }

        /** Is just starting and not already playing */
        if (turn == 0 && !is_playing_music) {
            is_playing_music = 1;
//            tokyo_drift();
        }
        /** Has completed the last turn in one lap */
        if (turn % 4 == 0 && lap >= 1) {
//            say_one_done();
        }

        CORO_YIELD();
    }
    CORO_END();
}
