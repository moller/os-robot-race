//
// Created by Robert Møller on 22/01/2022.
//

#ifndef OS_ROBOT_RACE_MOTOR_SPEED_H
#define OS_ROBOT_RACE_MOTOR_SPEED_H
CORO_CONTEXT(handle_motor_speed);

CORO_DEFINE(handle_motor_speed);

extern int c_speed[2];
extern int t_speed[2];
#endif //OS_ROBOT_RACE_MOTOR_SPEED_H
