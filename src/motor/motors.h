//
// Created by Robert Møller on 22/01/2022.
//

#ifndef OS_ROBOT_RACE_MOTORS_H
#define OS_ROBOT_RACE_MOTORS_H

#include <stdint.h>

enum {
    L,
    R,
    C /* Catapult */
};

enum {
    CORRECT = 0,
    ADJUSTING = 1
};

extern uint8_t motor[3];  /* Sequence numbers of motors */
extern int max_speed;     /* Tacho motor max speed */

void motor_set_speed(int l_speed, int r_speed);

void motor_stop();

int motor_is_running();

void launch_catapult();

#endif //OS_ROBOT_RACE_MOTORS_H
