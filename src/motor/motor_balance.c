//
// Created by Robert Møller on 22/01/2022.
//

#ifndef floorf

#include "math.h"

#endif

#include <stdio.h>
#include "coroutine.h"
#include "motors.h"
#include "motor_speed.h"
#include "../sensor/gyroscope.h"
#include "../flow/commands.h"
#include "motor_balance.h"


#define ANGLE_SLACK_DEG  0.0f

/**
 * @author Robert Moller
 * @details This code adjusts the balance between the left and right motors in order to make the difference between the
 * current angle `c_angle` and the target angle `t_angle` zero, either by slightly adjusting the speed or by turning drastically.
 */
CORO_DEFINE(handle_motor_balance) {
    CORO_LOCAL float d_angle, absolute_d_angle, how_sharp_turn, how_sharp_turn_remainder, min;
    CORO_LOCAL int adjust_left_right = CORRECT;
    CORO_LOCAL int linear_speed, turning_speed, adjusted_speed;
    CORO_BEGIN();

    linear_speed = (int) floorf(LINEAR_CAPACITY * (float) max_speed);
    turning_speed = (int) floorf(ANGULAR_CAPACITY * (float) linear_speed);

    for (;;) {
        /** If the current angle is outside of the target angle with some slack */
        if (t_angle - ANGLE_SLACK_DEG > c_angle || c_angle > t_angle + ANGLE_SLACK_DEG) {
            adjust_left_right = ADJUSTING;
            d_angle = t_angle - c_angle;
            absolute_d_angle = fabsf(floorf(d_angle));
            min = 0.000001f;
            how_sharp_turn = fminf(absolute_d_angle / HARD_TURN_DEG, 1.0f);
            how_sharp_turn_remainder = fmaxf(1.0f - how_sharp_turn, min);

            if (action == MOVE_FORWARD) {
                adjusted_speed = linear_speed - (int) floorf((float) turning_speed * how_sharp_turn);
                if (d_angle > 0) {
                    t_speed[L] = linear_speed;
                    t_speed[R] = adjusted_speed;
                } else {
                    t_speed[L] = adjusted_speed;
                    t_speed[R] = linear_speed;
                }
            } else if (action == TURN_ANGLE) {
                adjusted_speed = turning_speed - (int) floorf((float) turning_speed * how_sharp_turn_remainder);
                if (d_angle > 0) {
                    t_speed[L] = adjusted_speed;
                    t_speed[R] = -adjusted_speed;
                } else {
                    t_speed[L] = -adjusted_speed;
                    t_speed[R] = adjusted_speed;
                }
            } else if (action == STEP_BACKWARD) {
                adjusted_speed = turning_speed - (int) floorf((float) turning_speed * how_sharp_turn_remainder);
                if (d_angle > 0) {
                    t_speed[L] = adjusted_speed;
                    t_speed[R] = -adjusted_speed;
                } else {
                    t_speed[L] = -adjusted_speed;
                    t_speed[R] = adjusted_speed;
                }
            }
            printf("speed: (%d, %d)", t_speed[L], t_speed[R]);
        } else if (adjust_left_right != CORRECT) {
            adjust_left_right = CORRECT;
            command = MOVE_FORWARD;
            if (command != TURN_ANGLE) {
                o_angle = o_angle + c_angle;
                t_angle = c_angle = 0;
            }

        } else if (command != STEP_BACKWARD) {
            t_speed[L] = linear_speed;
            t_speed[R] = linear_speed;
        }
        CORO_YIELD();
    }
    CORO_END();
}