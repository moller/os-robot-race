

#include "motors.h"
#include "../helper/ev3dev.h"


uint8_t motor[3] = {DESC_LIMIT, DESC_LIMIT, DESC_LIMIT};
int max_speed;

/**
 * @author @in4lio (https://github.com/in4lio/ev3dev-c/blob/stretch/eg/drive/drive.c)
 * @detail set the speed for the left and left motors permanently
 */
void motor_set_speed(int l_speed, int r_speed) {
    set_tacho_speed_sp(motor[L], l_speed);
    set_tacho_speed_sp(motor[R], r_speed);
    multi_set_tacho_command_inx(motor, TACHO_RUN_FOREVER);
}

/**
 * @author @in4lio (https://github.com/in4lio/ev3dev-c/blob/stretch/eg/drive/drive.c)
 * @details Is the motor running @return 1 if rue, otherwise @return 0 if false
 */
int motor_is_running() {
    FLAGS_T state = TACHO_STATE__NONE_;
    get_tacho_state_flags(motor[L], &state);
    if (state != TACHO_STATE__NONE_) return (1);
    get_tacho_state_flags(motor[R], &state);
    if (state != TACHO_STATE__NONE_) return (1);
    return (0);
}

/**
 * @author @in4lio (https://github.com/in4lio/ev3dev-c/blob/stretch/eg/drive/drive.c)
 * @details Stop the motor
 */
void motor_stop() {
    multi_set_tacho_command_inx(motor, TACHO_STOP);
}