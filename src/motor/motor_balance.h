//
// Created by Robert Møller on 22/01/2022.
//

#ifndef OS_ROBOT_RACE_MOTOR_BALANCE_H
#define OS_ROBOT_RACE_MOTOR_BALANCE_H
CORO_CONTEXT(handle_motor_balance);

CORO_DEFINE(handle_motor_balance);

#define LINEAR_CAPACITY 0.90f
#define ANGULAR_CAPACITY 0.6f
#define HARD_TURN_DEG 45 // todo: try 45, 60 and 90
#endif //OS_ROBOT_RACE_MOTOR_BALANCE_H
