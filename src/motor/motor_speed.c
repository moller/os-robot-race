

#include "coroutine.h"
#include "motors.h"
#include "motor_speed.h"

int c_speed[2] = {0, 0};
int t_speed[2] = {0, 0};

/**
 * @author Robert Moller
 * @details Should match the current speed `c_speed` to any targeted speed `t_speed`
 */
CORO_DEFINE(handle_motor_speed) {
    CORO_BEGIN();
    for (;;) {
        CORO_WAIT(c_speed[L] != t_speed[L] || c_speed[R] != t_speed[R]);
        motor_set_speed(t_speed[L], t_speed[R]);
        c_speed[L] = t_speed[L];
        c_speed[R] = t_speed[R];
        CORO_YIELD();
    }
    CORO_END();
}
