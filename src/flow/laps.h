

#ifndef OS_ROBOT_RACE_LAPS_H
#define OS_ROBOT_RACE_LAPS_H

enum {
    NARROW = 1,
    BROAD = 3,
};
enum {
    MALICIOUS,
    TIME
};

extern int time;           /* Count of command iterations */
extern int start_position; /* Starting position, thick or narrow side*/
extern int race_mode; /* Starting position, thick or narrow side*/

int should_increase_safe_distance();

int is_short_side();

int laps_driven();
int turns_taken();

#endif //OS_ROBOT_RACE_LAPS_H
