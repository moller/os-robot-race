#ifndef printf

#include <stdio.h>

#endif

#include "coroutine.h"
#include "state.h"
#include "laps.h"
#include "../motor/motors.h"
#include "../motor/motor_speed.h"
#include "../motor/motor_balance.h"
#include "../sensor/gyroscope.h"
#include "commands.h"

int action;
int command;

/**
 * @author Robert Moller
 * @details @return string representation of the @param command enum
 */
const char *command_name(int cmd) {
    switch (cmd) {
        case MOVE_FORWARD:
            return "MOVE_FORWARD";
        case MOVE_NONE:
            return "MOVE_NONE";
        case TURN_ANGLE:
            return "TURN_ANGLE";
        case TURN_RIGHT:
            return "TURN_RIGHT";
        case STEP_BACKWARD:
            return "STEP_BACKWARD";
        case STOP:
            return "STOP";
        default:
            return "unknown";
    }
}


/**
 * @author Robert Moller
 * @details Coordinates commands to their actions
 */
CORO_DEFINE(handle_command) {
    CORO_BEGIN();
    for (;;) {
        CORO_WAIT(action != command);
        switch (command) {
            case MOVE_FORWARD:
                printf("move_forward");
                t_angle = 0;
                break;

            case TURN_LEFT:
                printf("turn_left");
                t_angle = -90;
                break;

            case TURN_RIGHT:
                printf("turn_right");
                t_angle = 90;
                break;

            case TURN_ANGLE:
                printf("turn_angle");
                break;

            case MOVE_NONE:
                motor_stop();
                t_speed[L] = 0;
                t_speed[R] = 0;
                CORO_WAIT(!motor_is_running());
                break;

            case STOP:
                printf("stop");
                motor_stop();
                CORO_WAIT(!motor_is_running());
                is_alive = DEAD;
                break;

            case STEP_BACKWARD:
                t_angle = -90;
                break;
        }
        action = command;
        CORO_YIELD();
    }
    CORO_END();
}