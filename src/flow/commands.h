

#ifndef OS_ROBOT_RACE_COMMANDS_H
#define OS_ROBOT_RACE_COMMANDS_H
CORO_CONTEXT(handle_command);

CORO_DEFINE(handle_command);

extern int action;  /* Current action */
extern int command; /* Command for the 'drive' coroutine */

const char *command_name(int cmd);

enum {
    MOVE_NONE,
    MOVE_FORWARD,
    MOVE_BACKWARD,
    TURN_LEFT,
    TURN_RIGHT,
    TURN_ANGLE,
    STEP_BACKWARD,

    DRIVE,
    /* Stopping */
    STOP,
    /* Gimmicks */
    BEEP,
};

#endif //OS_ROBOT_RACE_COMMANDS_H
