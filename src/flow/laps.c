

#include "laps.h"
#include "math.h"
#include "../sensor/gyroscope.h"

int time = 0;
int start_position = BROAD;
int race_mode = TIME;

/**
 * @author Robert Moller
 * @details If it's currently on the turn going to the broad side then the robot should increase the distance to start turning
 */
int should_increase_safe_distance() {
    if (turns_taken() % 4 == start_position) return 1;
    return 0;
}

/**
 * @author Robert Moller
 * @details Is the robot driving on the short side of the map
 */
int is_short_side() {
    return turns_taken() % 2;
}

/**
 * @author Robert Moller
 * @details Calculate how many laps the robot has driven
 */
int laps_driven() {
    return turns_taken() / 4;
}

/**
 * @author Robert Moller
 * @details Calculate how many times the robot has turned 90º
 */
int turns_taken() {
    return (int) fabs(o_angle / 90);
}