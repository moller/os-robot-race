/**
 * @author @in4lio
 * @detail https://github.com/in4lio/ev3dev-c/blob/stretch/eg/drive/drive.c
 */


#ifndef printf

#include <stdio.h>

#endif

#include "../helper/ev3dev.h"
#include "../motor/motors.h"
#include "commands.h"
#include "../sensor/sensors.h"
#include "state.h"

#define L_MOTOR_PORT      OUTPUT_C
#define L_MOTOR_EXT_PORT  EXT_PORT__NONE_
#define R_MOTOR_PORT      OUTPUT_B
#define R_MOTOR_EXT_PORT  EXT_PORT__NONE_
#define C_MOTOR_PORT      OUTPUT_A
#define C_MOTOR_EXT_PORT  EXT_PORT__NONE_


int is_alive;
/**
 * @author inspiired by @in4lio
 * @details search for engines an sensors
 */
int initialise_application() {
    ev3_sensor_init();
    ev3_tacho_init();

    /* Initialising state */
    command = action = MOVE_NONE;

    /* Initialising motors */
    if (ev3_search_tacho_plugged_in(L_MOTOR_PORT, L_MOTOR_EXT_PORT, motor + L, 0)) {
        get_tacho_max_speed(motor[L], &max_speed);
        set_tacho_command_inx(motor[L], TACHO_RESET);
    } else {
        printf("LEFT motor is NOT found.\n");
        return DEAD;
    }
    if (ev3_search_tacho_plugged_in(R_MOTOR_PORT, R_MOTOR_EXT_PORT, motor + R, 0)) {
        set_tacho_command_inx(motor[R], TACHO_RESET);
    } else {
        printf("RIGHT motor is NOT found.\n");
        return DEAD;
    }

    if (ev3_search_tacho_plugged_in(C_MOTOR_PORT, C_MOTOR_EXT_PORT, motor + C, 0)) {
        set_tacho_command_inx(motor[C], TACHO_RESET);
    } else {
        printf("CATAPULT motor is NOT found.\n");
        return DEAD;
    }

    /* Initialising sensors */
    if (ev3_search_sensor(LEGO_EV3_US, &sn_ir, 0)) {
        printf("IR sensor is found (%d).\n", sn_ir);
        set_sensor_mode_inx(sn_ir, US_US_DIST_CM);
    } else {
        printf("IR sensor is NOT found.\n");
        return DEAD;
    }

    if (ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro, 0)) {
        printf("Gyro sensor is found (%d).\n", sn_gyro);
        set_sensor_mode_inx(sn_gyro, GYRO_GYRO_RATE);
        set_sensor_mode_inx(sn_gyro, GYRO_GYRO_ANG);
    } else {
        printf("Gyro sensor is NOT found.\n");
        return DEAD;
    }
    return ALIVE;
}