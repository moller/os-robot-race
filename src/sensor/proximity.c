#ifndef CORO_DEFINE

#include "coroutine.h"

#endif
#ifndef fminf

#include <math.h>

#endif
#ifndef get_sensor_value0

#include "../helper/ev3dev.h"

#endif

#include <stdio.h>
#include "../flow/laps.h"
#include "sensors.h"
#include "../flow/commands.h"
#include "proximity.h"
#include "gyroscope.h"
#include "../motor/motors.h"
#include "../motor/motor_balance.h"
#include "../motor/motor_speed.h"
#include "../gimmick/speech.h"

#define PI 3.141592654f

// maybe even keep track of when last turn was to see if it is likely to be in a wall
// keep track of what length we are on; we can use this to know around what relative time its relevant to turn
// also we could use this info to celebrate on the finish line, do a dance, play a song or something

float c_prox;

/**
 * Converts @param degrees to @return radians
 * @author Robert Moller
 */
float deg_to_rad(float deg) {
    return (deg * PI) / 180;
}

/**
 * Converts @param radians to @return degrees
 * @author Robert Moller
 */
float rad_to_deg(float rad) {
    return (rad * 180) / PI;
}

/**
 * Calculate the hypotenuse of a right angled triangle given
 * @param a the length of the base side
 * @param a_angle the angle between the base and hypotenuse sides
 * @author Robert Moller
 */
float expected_hyp(float a, float a_angle) {
    return a / sinf(a_angle);
}

/**
 * Calculate the opposite side of a right angled triangle given
 * @param adj the length of the adjacent side
 * @param hyp the length of the hypotenuse side
 * @author Robert Moller
 */
float opposite_side(float adj, float hyp) {
    return sqrtf(powf(hyp, 2.0f) - powf(adj, 2.0f));
}

/**
 * Calculate the @return hypotenuse given
 * @param adjacent side length
 * @param opposite side length
 * @author Robert Moller
 */
float hypotenus_side(float adj, float opposite) {
    return sqrtf(powf(opposite, 2.0f) + powf(adj, 2.0f));
}

/**
 * Measures the proximity to any object directly in front of the robot.
 * This measurement is stored as the current proximity `c_prox` for reading by other parts of the code
 * @author Robert Moller
 */
CORO_DEFINE(measure_ir_proximity) {
    CORO_BEGIN();
    for (;;) {
        if (!get_sensor_value0(sn_ir, &c_prox)) c_prox = 0;
        if (c_prox < DANGER_DISTANCE || c_prox > 2500) {
            printf("danger\t");
        } else if (c_prox < SAFE_DISTANCE) {
            printf("warning\t");
        } else {
            printf("safe\t");
        }

        printf("c_prox: %4.0f\t", c_prox);
        CORO_YIELD();
    }
    CORO_END();
}

/**
 * @details decides when and what action should be taken given a ceetain proximity from the front of the robot.
 * @author @in4lio (https://github.com/in4lio/ev3dev-c/blob/stretch/eg/drive/drive.c), Robert Moller, Team Discussion
 */
CORO_DEFINE(handle_ir_proximity) {
    CORO_LOCAL float center_prox, right_prox, left_prox, turn_distance, expected_hypotenuse,
            actual_hypotenuse, d_hypotenuse, expected_opposite, actual_opposite, C_angle, B_angle, A_angle, a, b, c;
    CORO_LOCAL int t_time, w_time;
    CORO_BEGIN();

    for (;;) {
        /** If we are on the narrow side, then use a decreased distance at which robot should start turning */
        turn_distance = (is_short_side() ? SAFE_DISTANCE_SHORT_SIDE : SAFE_DISTANCE) +
                        (should_increase_safe_distance() ? SAFE_DISTANCE : 0.0f);
        /** Either close or real up close */
        if (c_prox < DANGER_DISTANCE || 2000 <= c_prox) {
            command = STEP_BACKWARD;
//            say_ouch();
            CORO_WAIT(command != STEP_BACKWARD);
            o_angle = o_angle + c_angle;
            c_angle = t_angle = 0;

        } else if (c_prox < turn_distance) {
            t_angle = -20;
            C_angle = fabsf(t_angle);
            motor_stop();
            CORO_WAIT(!motor_is_running());
            center_prox = c_prox;
            /** This calculation was used to determine when to avoid objects and when to treat it like wall, but is now replaced with something yielding better results  */
            expected_hypotenuse = expected_hyp(center_prox, t_angle);
            expected_opposite = opposite_side(center_prox, expected_hypotenuse);
            command = TURN_ANGLE;
            CORO_WAIT(command == MOVE_FORWARD);
            left_prox = c_prox;
            actual_hypotenuse = left_prox;
            d_hypotenuse = actual_hypotenuse - expected_hypotenuse;
            actual_opposite = hypotenus_side(d_hypotenuse, expected_opposite);

            /**
             * Calculate the remaining angles for the given
             * @param adjacent side b
             * @param hyponetnuse side a
             * @param angle C_angle
             * @return angles A_angle and B_angle
             */
            b = center_prox;
            a = left_prox;
            float b_2 = powf(b, 2.0f);
            float a_2 = powf(a, 2.0f);
            float cos_c = cosf(deg_to_rad(C_angle));
            float ba2cos_c = 2.0f * b * a * cos_c;
            c = sqrtf(b_2 + a_2 - ba2cos_c);
            A_angle = rad_to_deg(acosf((powf(c, 2.0f) + powf(b, 2.0f) - powf(a, 2.0f)) / (2.0f * c * b)));
            B_angle = 180 - A_angle - C_angle;

            float measurement_slack = 50.0f;
//            float measurement_slack = 20.0f; // worked good
            float front_wall_dist = fabsf(expected_hypotenuse) + measurement_slack;
            float side_wall_dist = fabsf(expected_hypotenuse) - measurement_slack;
            printf("left_prox: %3f\tcenter_prox: %3.2f\tdiff: %3f\t", left_prox, center_prox, left_prox - center_prox);


            /** If the left proximity is within the range of slack to the center proximity, assume it is a wall and turn left */
            if (center_prox - measurement_slack < left_prox && left_prox < center_prox + measurement_slack) {
                t_angle = -90 + C_angle;
                command = TURN_ANGLE;
                CORO_WAIT(command == MOVE_FORWARD);
            } /** If the left proximity is beyond the range of slack to the center proximity, assume it is an obstacle with free path to the left */
            else if (center_prox - measurement_slack < left_prox && center_prox + measurement_slack < left_prox) {
                command = MOVE_FORWARD;
                t_time = 0;
                w_time = 0.7f * 1000;
                for (t_time = 0; t_time * 10 < w_time && c_prox > DANGER_DISTANCE; t_time++) {
                    CORO_YIELD();
                }
                t_angle = C_angle;
                command = TURN_ANGLE;
                CORO_WAIT(command == MOVE_FORWARD);
            } /** If the left proximity is shorter than the range of slack to the center proximity, assume it is an obstacle with no free path to the left */
            else if (left_prox < center_prox - measurement_slack) {
                motor_stop();
                CORO_WAIT(!motor_is_running());
                t_angle = C_angle * 2;
                command = TURN_ANGLE;
                CORO_WAIT(command == MOVE_FORWARD);
                right_prox = c_prox;
//                printf("right_prox: %3.2f\tfront_wall_dist: %3.2f\tside_wall_dist: %3.2f\t", right_prox,
//                       front_wall_dist,
//                       side_wall_dist);
                /** If the rigth proximity is beyond the expected distance to a wall, assume it is an obstacle with free path to the right */
                if (front_wall_dist < right_prox) {
                    command = MOVE_FORWARD;
                    t_time = 0;
                    w_time = 0.7f * 1000;
                    for (t_time = 0; t_time * 10 < w_time && c_prox > DANGER_DISTANCE; t_time++) {
                        CORO_YIELD();
                    }
                    t_angle = -C_angle;
                    command = TURN_ANGLE;
                    CORO_WAIT(command == MOVE_FORWARD);
                }

            }
        } else {
            printf("safe\t");
            command = MOVE_FORWARD;
        }

        CORO_YIELD();
    }
    CORO_END();
}