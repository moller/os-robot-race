//
// Created by Robert Møller on 22/01/2022.
//

#ifndef CORO_DEFINE

#include "coroutine.h"

#endif

#ifndef fmodf

#include <math.h>

#endif

#ifndef get_sensor_value0

#include "../helper/ev3dev.h"

#endif


#include <stdio.h>
#include "sensors.h"
#include "gyroscope.h"

#define ROTATION_DEG 360.0f

float o_angle; /* Offset angle of rotation */
float c_angle; /* Current angle of rotation */
float t_angle; /* Target angle of rotation */

/**
 * @author Robert Moller
 * Read and store gyroscope data
 */
CORO_DEFINE(handle_gyroscope) {
    CORO_LOCAL float i_angle; /* Initial angle */
    CORO_LOCAL float m_angle; /* Measured angle */
    CORO_BEGIN();

    get_sensor_value0(sn_gyro, &i_angle);
    o_angle = 0;
    for (;;) {
        if (!get_sensor_value0(sn_gyro, &m_angle)) m_angle = 0.0f;
        c_angle = fmodf(m_angle - i_angle - o_angle, ROTATION_DEG);
        printf("c_angle: %3.0fº -> %3.0fº\t", c_angle, t_angle);
        CORO_YIELD();
    }
    CORO_END();
}
