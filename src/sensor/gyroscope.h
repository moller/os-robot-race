//
// Created by Robert Møller on 22/01/2022.
//

#ifndef OS_ROBOT_RACE_GYROSCOPE_H
#define OS_ROBOT_RACE_GYROSCOPE_H
CORO_CONTEXT(handle_gyroscope);
CORO_DEFINE(handle_gyroscope);
extern float o_angle;
extern float c_angle;
extern float t_angle;
#endif //OS_ROBOT_RACE_GYROSCOPE_H
