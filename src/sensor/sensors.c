//
// Created by Robert Møller on 22/01/2022.
//
#include <stdint.h>

#include "sensors.h"

uint8_t sn_ir;      /* Sensor reference for the IR sensor */
uint8_t sn_gyro;    /* Sensor reference for the gyroscope sensor */
