//
// Created by Robert Møller on 22/01/2022.
//

#ifndef OS_ROBOT_RACE_PROXIMITY_H
#define OS_ROBOT_RACE_PROXIMITY_H

CORO_CONTEXT(measure_ir_proximity);
CORO_CONTEXT(handle_ir_proximity);

CORO_DEFINE(measure_ir_proximity);

CORO_DEFINE(handle_ir_proximity);

#define SAFE_DISTANCE 300.0f
#define SAFE_DISTANCE_SHORT_SIDE 180.0f
#define DANGER_DISTANCE 100.0f

extern float c_prox;                    /* Current proximity*/

#endif //OS_ROBOT_RACE_PROXIMITY_H
