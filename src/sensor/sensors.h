//
// Created by Robert Møller on 22/01/2022.
//

#ifndef OS_ROBOT_RACE_SENSORS_H
#define OS_ROBOT_RACE_SENSORS_H

#include <stdint.h>

extern uint8_t sn_ir;
extern uint8_t sn_gyro;

#endif //OS_ROBOT_RACE_SENSORS_H
