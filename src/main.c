#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <math.h>

#include "coroutine.h"

#ifndef ev3_init

#include "helper/ev3dev.h"

#endif


/* Gimmicks */
#include "gimmick/beep.h"
#include "gimmick/gimmicks.h"
/* Motors */
#include "motor/motors.h"
#include "motor/motor_balance.h"
#include "motor/motor_speed.h"
/* Sensors */
#include "sensor/sensors.h"
#include "sensor/gyroscope.h"
#include "sensor/proximity.h"
/* Control flow*/
#include "flow/state.h"
#include "flow/commands.h"
#include "flow/laps.h"

/**
 *
 * @author Robert Moller
 */
void onExit(int code) {
    is_alive = 0;
    motor_stop();
    while (motor_is_running()) Sleep(100);
    printf("exit(%d)", code);
}

/**
 * @details Infinite loop iterating every 10ms over a list of coroutines handling
 * - Proximity measurement
 * - Making decisions based on the proximity
 * - Gyroscope measurement
 * - Deciding what gimmick to employ
 * - Balancing the left and right motors to achieve an angle
 * - Reacting to commands
 * - Update the speed to match the target speed
 * @author @in4lio (https://github.com/in4lio/ev3dev-c/blob/stretch/eg/drive/drive.c), Robert Moller
 */
int main() {
    signal(SIGINT, onExit);
    printf("Waiting the EV3 brick online...\n");
    if (ev3_init() < 1) return ERROR;
    printf("*** ( EV3 ) Hello! ***\n");

    is_alive = initialise_application();


    for (time = 0; is_alive; time++) {
        printf("[%6d] %s\t", time, command_name(action));

        CORO_CALL(measure_ir_proximity);
        CORO_CALL(handle_ir_proximity);
        CORO_CALL(handle_gyroscope);

        CORO_CALL(handle_gimmicks);

        CORO_CALL(handle_motor_balance);
        CORO_CALL(handle_command);
        CORO_CALL(handle_motor_speed);
        printf("\n");
        Sleep(10);
    }
    onExit(1);
    ev3_uninit();
    printf("*** ( EV3 ) Bye! ***\n");
    return OK;
}