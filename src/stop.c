//
// Created by Robert Møller on 22/01/2022.
//

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#include "coroutine.h"
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "ev3_tacho.h"

#define L_MOTOR_PORT      OUTPUT_C
#define L_MOTOR_EXT_PORT  EXT_PORT__NONE_
#define R_MOTOR_PORT      OUTPUT_B
#define R_MOTOR_EXT_PORT  EXT_PORT__NONE_
#define C_MOTOR_PORT      OUTPUT_A
#define C_MOTOR_EXT_PORT  EXT_PORT__NONE_

uint8_t motor[3] = {DESC_LIMIT, DESC_LIMIT, DESC_LIMIT};
int max_speed;

enum {
    L,
    R,
    C
};

static void motor_stop() {
    multi_set_tacho_command_inx(motor, TACHO_STOP);
}

void initialise_application() {
    ev3_tacho_init();

    if (ev3_search_tacho_plugged_in(L_MOTOR_PORT, L_MOTOR_EXT_PORT, motor + L, 0)) {
        get_tacho_max_speed(motor[L], &max_speed);
        set_tacho_command_inx(motor[L], TACHO_RESET);
    } else {
        printf("LEFT motor is NOT found.\n");
        return;
    }
    if (ev3_search_tacho_plugged_in(R_MOTOR_PORT, R_MOTOR_EXT_PORT, motor + R, 0)) {
        set_tacho_command_inx(motor[R], TACHO_RESET);
    } else {
        printf("RIGHT motor is NOT found.\n");
        return;
    }
    if (ev3_search_tacho_plugged_in(C_MOTOR_PORT, C_MOTOR_EXT_PORT, motor + R, 0)) {
        set_tacho_command_inx(motor[C], TACHO_RESET);
    } else {
        printf("CATAPULT motor is NOT found.\n");
        return;
    }
    return;
}

int main() {
    printf("Waiting the EV3 brick online...\n");
    if (ev3_init() < 1) return 0;
    printf("*** ( EV3 ) Hello! ***\n");
    initialise_application();
    motor_stop();
    ev3_uninit();
    printf("*** ( EV3 ) Bye! ***\n");
    return 0;
}