# OS Robot Race - Group 3

## Project
This project is part of the Operating Course 2021 at Eurecom.
We must configure a Lego Mindstorm EV3 robot in order to compete at a racing challenge.
The main objective of the challenge is to be the winner of three different races. "Winner" means being the fastest to reach the finish line. A race contains 2 laps. A lap ends at the finsh line.
The different races will be:
- Race against time: robot compete alone in the stadium. The robot starts at pos 1.
- Race against a distant opponent: two robots compete but do not start at the same location: robots start at position 1 and 3.
- Race against a proximate opponent: two robots compete and start next to each other, i.e. at position 1 and 2.



Your report consists of a website and the source code of your robot. 
The website shall contain the following information:
 * Description of the architecture of the robot: 
   * sensors and actuators, etc. 
   * Pictures of your robot on the web site would be appreciated.
 * Algorithms used for the two most important points: 
   * strategy for the different races. 
     * Don't provide source code here, just try to describe the algorithms using a pseudo C language. 
     * Also, do comment those algorithms, and explain why you think they are efficient.
 * Source code, and instructions on how to use that source code: 
   * how to compile it, 
   * how to download it on the robot, 
   * and how to start the robot. 
   * To protect you work, 
     * you can set a password to access to the code, but make sure to give us access in order to grade it (e.g., private git repository). 
     * You could even put a fake code on your website until the very last moment ;-). 
   * I strongly advise you to rely on a versioning system (svn, git, hg) to work on the code. Also, frequently backup your code.
 * Videos / pictures of your robot in action [we may provide you with cameras and video recorders if necessary].
 * How you have worked in the group, i.e., who made what. 
   * Be clear about that. Each member of groups will get an individual grade. 
   * All members of a team must contribute to the source code. 
   * You source code must clearly indicate, with comments, who programmed which function.

# Report
# Description of the Robot architecture
We have built the base model robot from the provided instruction manual with slight modifications.

# Installation
